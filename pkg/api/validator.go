/*
 * Go REST API
 *
 * API version: 1.0.0
 */

package api

import validator "gopkg.in/go-playground/validator.v9"

// validate - initializes validator
func validate() *validator.Validate {
	return validator.New()
}
