FROM golang:latest as build
WORKDIR /go/src/gitlab.com/dannietjoh/go-rest-api
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build

FROM alpine:latest
RUN adduser -D -g '' gouser && \
    mkdir -p /etc/ssl/certs && \
    mkdir -m 777 /config /data
COPY --chown=gouser config /config
COPY --from=build /go/src/gitlab.com/dannietjoh/go-rest-api/go-rest-api /
USER gouser
WORKDIR /
EXPOSE 8080/tcp
ENTRYPOINT ["/go-rest-api"]
