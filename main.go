/*
 * Go REST API
 *
 * API version: 1.0.0
 */

package main

import (
	"gitlab.com/dannietjoh/go-rest-api/pkg/api"
)

func main() {
	api.Run()
}
